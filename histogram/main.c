#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dump.h"
#include "utils.h"

#include <omp.h>

#define UINT8_maximum 255

int main(int argc, char* argv[]) {
  struct pb_TimerSet timers;
  struct pb_Parameters *parameters;

  parameters = pb_ReadParameters(&argc, argv);
  if (!parameters)
    return -1;

  if(!parameters->inpFiles[0]){
    fputs("Input file expected\n", stderr);
    return -1;
  }

  int numIterations;
  if (argc >= 2){
    numIterations = atoi(argv[1]);
  } else {
    fputs("Expected at least one command line argument\n", stderr);
    return -1;
  }

  pb_InitializeTimerSet(&timers);

  char *inputStr = "Input";
  char *outputStr = "Output";

  pb_AddSubTimer(&timers, inputStr, pb_TimerID_IO);
  pb_AddSubTimer(&timers, outputStr, pb_TimerID_IO);

  pb_SwitchToSubTimer(&timers, inputStr, pb_TimerID_IO);

  unsigned int img_width, img_height;
  unsigned int histo_width, histo_height;

  FILE* f = fopen(parameters->inpFiles[0],"rb");
  int result = 0;

  result += fread(&img_width,    sizeof(unsigned int), 1, f);
  result += fread(&img_height,   sizeof(unsigned int), 1, f);
  result += fread(&histo_width,  sizeof(unsigned int), 1, f);
  result += fread(&histo_height, sizeof(unsigned int), 1, f);

  if (result != 4){
    fputs("Error reading input and output dimensions from file\n", stderr);
    return -1;
  }

  unsigned int* img = (unsigned int*) malloc (img_width*img_height*sizeof(unsigned int));
  unsigned char* histo = (unsigned char*) calloc (histo_width*histo_height, sizeof(unsigned char));
  
  pb_SwitchToSubTimer(&timers, "Input", pb_TimerID_IO);

  result = fread(img, sizeof(unsigned int), img_width*img_height, f);

  fclose(f);

  if (result != img_width*img_height){
    fputs("Error reading input array from file\n", stderr);
    return -1;
  }

  pb_SwitchToTimer(&timers, pb_TimerID_COMPUTE);

    unsigned int minimum = img[0], maximum = img[0];

    int j;
#pragma omp parallel for reduction(min:minimum) reduction(max:maximum)
    for(j = 0; j < img_width*img_height*sizeof(unsigned char); j++){
        if(img[j] < minimum)
            minimum = img[j];
        if(img[j] > maximum)
            maximum = img[j];
    }

    unsigned int max_num_of_threads = 8;
    unsigned char ** buffer =  malloc(max_num_of_threads* sizeof(unsigned char*));

    int i;
    for(i = 0; i < max_num_of_threads; i++)
        buffer[i] = 0;

#pragma omp parallel shared(minimum, maximum, buffer)
    {
        int myID = omp_get_thread_num();
        buffer[myID] = malloc((maximum - minimum) * sizeof(unsigned char));
    }


  int iter;
  for (iter = 0; iter < numIterations; iter++){
    memset(histo,0,histo_height*histo_width*sizeof(unsigned char));
    unsigned int i;
    unsigned int myID;
#pragma omp parallel default(none) \
                     private(myID) \
                     shared(buffer, minimum, maximum, img_height, img_width, img)
{
    myID = omp_get_thread_num();
    memset(buffer[myID],0,maximum-minimum);
#pragma omp for
    for (i = 0; i < img_width*img_height; ++i) {
      const unsigned int value = img[i];
      if (buffer[myID][value - minimum] < UINT8_maximum) {
        ++buffer[myID][value - minimum];
      }
    }
}

    //reduction
    int thread, value;
    for(thread = 0;thread < max_num_of_threads; thread++) {
        if(buffer[thread]) {
            for(value = minimum; value < maximum; value++) {
                if(histo[value] + buffer[thread][value-minimum] < UINT8_maximum)
                    histo[value] = histo[value] + buffer[thread][value-minimum];
                else
                    histo[value] = UINT8_maximum;
            }
        }
    }

    } // for

#pragma omp parallel shared(minimum, maximum, buffer)
    {
        int myID = omp_get_thread_num();
        free(buffer[myID]);
    }

  free(buffer);

  pb_SwitchToSubTimer(&timers, outputStr, pb_TimerID_IO);

  if (parameters->outFile) {
    dump_histo_img(histo, histo_height, histo_width, parameters->outFile);
  }

  pb_SwitchToTimer(&timers, pb_TimerID_COMPUTE);

  free(img);
  free(histo);

  pb_SwitchToTimer(&timers, pb_TimerID_NONE);

  printf("\n");
  pb_PrintTimerSet(&timers);
  pb_FreeParameters(parameters);

  return 0;
}
