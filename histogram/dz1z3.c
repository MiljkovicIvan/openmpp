#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <omp.h>

#include "dump.h"
#include "utils.h"

#define UINT8_MAX 255

int main(int argc, char* argv[]) {
  struct pb_TimerSet timers;
  struct pb_Parameters *parameters;

  parameters = pb_ReadParameters(&argc, argv);
  if (!parameters)
    return -1;

  if(!parameters->inpFiles[0]){
    fputs("Input file expected\n", stderr);
    return -1;
  }

  int numIterations;
  if (argc >= 2){
    numIterations = atoi(argv[1]);
  } else {
    fputs("Expected at least one command line argument\n", stderr);
    return -1;
  }

  //sequential
  pb_InitializeTimerSet(&timers);

  char *inputStr = "Input";
  char *outputStr = "Output";

  pb_AddSubTimer(&timers, inputStr, pb_TimerID_IO);
  pb_AddSubTimer(&timers, outputStr, pb_TimerID_IO);

  pb_SwitchToSubTimer(&timers, inputStr, pb_TimerID_IO);

  unsigned int img_width, img_height;
  unsigned int histo_width, histo_height;

  FILE* f = fopen(parameters->inpFiles[0],"rb");
  int result = 0;

  result += fread(&img_width,    sizeof(unsigned int), 1, f);
  result += fread(&img_height,   sizeof(unsigned int), 1, f);
  result += fread(&histo_width,  sizeof(unsigned int), 1, f);
  result += fread(&histo_height, sizeof(unsigned int), 1, f);

  if (result != 4){
    fputs("Error reading input and output dimensions from file\n", stderr);
    return -1;
  }

  unsigned int* img = (unsigned int*) malloc (img_width*img_height*sizeof(unsigned int));
  unsigned char* histo_sec = (unsigned char*) calloc (histo_width*histo_height, sizeof(unsigned char));

  pb_SwitchToSubTimer(&timers, "Input", pb_TimerID_IO);

  result = fread(img, sizeof(unsigned int), img_width*img_height, f);

  fclose(f);

  if (result != img_width*img_height){
    fputs("Error reading input array from file\n", stderr);
    return -1;
  }

  pb_SwitchToTimer(&timers, pb_TimerID_COMPUTE);

  int iter;
  for (iter = 0; iter < numIterations; iter++){
    memset(histo_sec,0,histo_height*histo_width*sizeof(unsigned char));
    unsigned int i;
    for (i = 0; i < img_width*img_height; ++i) {
      const unsigned int value = img[i];
      if (histo_sec[value] < UINT8_MAX) {
        ++histo_sec[value];
      }
    }
  }

  pb_SwitchToSubTimer(&timers, outputStr, pb_TimerID_IO);

  if (parameters->outFile) {
    dump_histo_img(histo_sec, histo_height, histo_width, parameters->outFile);
  }

  pb_SwitchToTimer(&timers, pb_TimerID_COMPUTE);

  free(img);

  pb_SwitchToTimer(&timers, pb_TimerID_NONE);

  pb_PrintTimerSet(&timers);

  //parallel

  printf("Start of parallel execution.\n");


  pb_InitializeTimerSet(&timers);


  pb_AddSubTimer(&timers, inputStr, pb_TimerID_IO);
  pb_AddSubTimer(&timers, outputStr, pb_TimerID_IO);

  pb_SwitchToSubTimer(&timers, inputStr, pb_TimerID_IO);


  f = fopen(parameters->inpFiles[0],"rb");
  result = 0;

  result += fread(&img_width,    sizeof(unsigned int), 1, f);
  result += fread(&img_height,   sizeof(unsigned int), 1, f);
  result += fread(&histo_width,  sizeof(unsigned int), 1, f);
  result += fread(&histo_height, sizeof(unsigned int), 1, f);

  if (result != 4){
    fputs("Error reading input and output dimensions from file\n", stderr);
    return -1;
  }

  img = (unsigned int*) malloc (img_width*img_height*sizeof(unsigned int));
  unsigned char* histo_par = (unsigned char*) calloc (histo_width*histo_height, sizeof(unsigned char));

  pb_SwitchToSubTimer(&timers, "Input", pb_TimerID_IO);

  result = fread(img, sizeof(unsigned int), img_width*img_height, f);

  fclose(f);

  if(result != img_width*img_height) {
      fputs("Error reading input array from file\n", stderr);
      return -1;
  }

  pb_SwitchToTimer(&timers, pb_TimerID_COMPUTE);

  unsigned int histo_length = 0, histo_length_full = histo_width * histo_height, chunk_full = 0, chunk_reduced = 0;
  unsigned int num_threads = 0;
  unsigned char **histo_buffer = NULL;
  unsigned int min = ~0, max = 0;

  #pragma omp parallel default(none)\
                       shared(histo_buffer, histo_length, num_threads,\
                              histo_par, img, img_width, img_height,\
                              min, max, chunk_reduced, chunk_full,\
                              histo_length_full)
  {
       unsigned int i = 0;

       #pragma omp for schedule(static) reduction(max : max) reduction(min : min)
       for (i = 0; i < img_width * img_height; i++) {
           if (img[i] < min) {
               min = img[i];
           }
           if (img[i] > max) {
               max = img[i];
           }
       }

       #pragma omp single
       {
           histo_length = (max - min + 1);
           num_threads = omp_get_num_threads();

           histo_buffer = (unsigned char**) malloc(num_threads * sizeof(unsigned char*));
           for (i = 0; i < num_threads; i++) {
               histo_buffer[i] = (unsigned char*) malloc(histo_length * sizeof(unsigned char));
           }

           chunk_reduced = (histo_length + num_threads - 1) / num_threads;
           chunk_full = (histo_length_full + num_threads - 1) / num_threads;
       }
  }

  for (iter = 0; iter < numIterations; iter++) {
   #pragma omp parallel default(none)\
                        shared(histo_buffer, img_width, img, img_height,\
                               histo_length, histo_length_full, chunk_full,\
                               chunk_reduced, num_threads, min, histo_par)
   {
       int my_id = omp_get_thread_num();
       unsigned int i = 0;


       unsigned int start_full = my_id * chunk_full, chunk;
       if ((start_full + chunk_full) > histo_length_full) {
           chunk = histo_length_full - start_full;
       } else {
           chunk = chunk_full;
       }

       memset(&histo_par[start_full], 0, chunk);

       memset(histo_buffer[my_id], 0, histo_length);

       #pragma omp for schedule(static)
       for (i = 0; i < img_width*img_height; ++i) {
           unsigned int value = img[i];
           value -= min;

         if (histo_buffer[my_id][value] < UINT8_MAX) {
         ++histo_buffer[my_id][value];
         }
       }

       //reduction
       unsigned int j = 0;
       for (i = 0; i < num_threads; i++) {
           #pragma omp for schedule(static)
           for (j = 0; j < histo_length; j++) {
               histo_par[j + min] = (histo_par[j + min] + histo_buffer[i][j]) > UINT8_MAX
                               ? UINT8_MAX :
                                 (histo_par[j + min] + histo_buffer[i][j]);
           }
       }
    }
 }

  unsigned int i = 0;
  for(i = 0; i < num_threads; i++) {
      free(histo_buffer[i]);
  }
  free(histo_buffer);

  pb_SwitchToSubTimer(&timers, outputStr, pb_TimerID_IO);

  if (parameters->outFile) {
       dump_histo_img(histo_par, histo_height, histo_width, parameters->outFile);
  }

  pb_SwitchToTimer(&timers, pb_TimerID_COMPUTE);

  free(img);

  pb_SwitchToTimer(&timers, pb_TimerID_NONE);

  printf("\n");
  pb_PrintTimerSet(&timers);


  // end of parallel execution

  pb_FreeParameters(parameters);


  if(memcmp(histo_sec, histo_par, histo_height*histo_width) != 0) {
      printf("\nTest FAILED\n");
  } else {
      printf("\nTest PASSED\n");
  }

  free(histo_sec);

  return 0;
}
