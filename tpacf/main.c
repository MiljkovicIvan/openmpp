#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <sys/time.h>
#include <math.h>

#include <omp.h>

#include "args.h"
#include "model.h"

int main( int argc, char **argv )
{
  struct pb_TimerSet timers;
  struct pb_Parameters *params;
  int rf, k, nbins, npd, npr;
  float *binb, w;
  long long *DD, *RRS, *DRS;
  size_t memsize;
  struct cartesian *data, *random;
  FILE *outfile;

  pb_InitializeTimerSet( &timers );
  params = pb_ReadParameters( &argc, argv );

  options args;
  parse_args( argc, argv, &args );

  pb_SwitchToTimer( &timers, pb_TimerID_COMPUTE );
  nbins = (int)floor(bins_per_dec * (log10(max_arcmin) -
                     log10(min_arcmin)));
  memsize = (nbins+2)*sizeof(long long);

  // memory for bin boundaries
  binb = (float *)malloc((nbins+1)*sizeof(float));
  if (binb == NULL)
    {
      fprintf(stderr, "Unable to allocate memory\n");
      exit(-1);
    }
  for (k = 0; k < nbins+1; k++)
    {
      binb[k] = cos(pow(10, log10(min_arcmin) +
            k*1.0/bins_per_dec) / 60.0*D2R);
    }

  // memory for DD
  DD = (long long*)malloc(memsize);
  if (DD == NULL)
    {
      fprintf(stderr, "Unable to allocate memory\n");
      exit(-1);
    }
  bzero(DD, memsize);

  // memory for RR
  RRS = (long long*)malloc(memsize);
  if (RRS == NULL)
    {
      fprintf(stderr, "Unable to allocate memory\n");
      exit(-1);
    }
  bzero(RRS, memsize);

  // memory for DR
  DRS = (long long*)malloc(memsize);
  if (DRS == NULL)
    {
      fprintf(stderr, "Unable to allocate memory\n");
      exit(-1);
    }
  bzero(DRS, memsize);

  // memory for input data
  data = (struct cartesian*)malloc
    (args.npoints* sizeof(struct cartesian));
  if (data == NULL)
    {
      fprintf(stderr,
          "Unable to allocate memory for % data points (#1)\n",
          args.npoints);
      return(0);
    }

  random = (struct cartesian*)malloc
    (args.npoints*sizeof(struct cartesian));
  if (random == NULL)
    {
      fprintf(stderr,
          "Unable to allocate memory for % data points (#2)\n",
          args.npoints);
      return(0);
    }

  printf("Min distance: %f arcmin\n", min_arcmin);
  printf("Max distance: %f arcmin\n", max_arcmin);
  printf("Bins per dec: %i\n", bins_per_dec);
  printf("Total bins  : %i\n", nbins);

  // read data file
  pb_SwitchToTimer( &timers, pb_TimerID_IO );
  npd = readdatafile(params->inpFiles[0], data, args.npoints);
  pb_SwitchToTimer( &timers, pb_TimerID_COMPUTE );
  if (npd != args.npoints)
    {
      fprintf(stderr,
          "Error: read %i data points out of %i\n",
          npd, args.npoints);
      return(0);
    }

  // compute DD
  doCompute(data, npd, NULL, 0, 1, DD, nbins, binb);

  //TODO:
  // - izbaciti ucitavanje u fajl van petlje i paralelizovati ga
  // - napraviti poseble RRS i DRS i redukovati ih
  // loop through random data files

  struct cartesian **random_buffer;
  random_buffer = (struct cartesian**)malloc(args.random_count*sizeof(struct cartesian *));
  int ERROR = 0, ERROR_npr;
  int * npr_buffer;
  char* ERROR_inpFile;
  npr_buffer = (int *)malloc(args.random_count*sizeof(int));

  pb_SwitchToTimer( &timers, pb_TimerID_IO );
#pragma omp parallel for
  for(rf = 0; rf < args.random_count; rf++) {
    random_buffer[rf] = (struct cartesian*)malloc(args.npoints*sizeof(struct cartesian));
      npr_buffer[rf] = readdatafile(params->inpFiles[rf+1], random_buffer[rf], args.npoints);

      if (npr_buffer[rf] != args.npoints)
        {
            ERROR = 1;
            ERROR_npr = npr;
            ERROR_inpFile = params->inpFiles[rf+1];
        }
  }
  pb_SwitchToTimer( &timers, pb_TimerID_COMPUTE );

  if(ERROR) {
      fprintf(stderr,
          "Error: read %i random points out of %i in file %s\n",
          ERROR_npr, args.npoints, ERROR_inpFile);
      return(0);
  }

  int myID, total_threads;
  long long **RRS_buffer, **DRS_buffer;

#pragma omp parallel private(myID) \
                     shared(RRS_buffer, DRS_buffer, total_threads)
{
    // allocate RRS i DRS for each thread
    myID = omp_get_thread_num();
    total_threads = omp_get_num_threads();

#pragma omp single
    {
        RRS_buffer = (long long **)malloc(total_threads*sizeof(long long*));
        DRS_buffer = (long long **)malloc(total_threads*sizeof(long long*));
        int thread;
        for(thread = 0; thread < total_threads; thread++) {
            RRS_buffer[thread] = (long long *)malloc(memsize);
            bzero(RRS_buffer[thread], memsize);

            DRS_buffer[thread] = (long long *)malloc(memsize);
            bzero(DRS_buffer[thread], memsize);
        }
    }

#pragma omp barrier

#pragma omp for
  for (rf = 0; rf < args.random_count; rf++)
    {
      // read random file

      // compute RR
      doCompute(random, npr_buffer[rf], NULL, 0, 1, RRS_buffer[myID], nbins, binb);

      // compute DR
      doCompute(data, npd, random, npr_buffer[rf], 0, DRS_buffer[myID], nbins, binb);
    }

#pragma omp master
  {
  //reduce each DRS and RRS array into one
  }
}

  // compute and output results
  if ((outfile = fopen(params->outFile, "w")) == NULL)
    {
      fprintf(stderr,
          "Unable to open output file %s for writing, assuming stdout\n",
          params->outFile);
      outfile = stdout;
    }

  pb_SwitchToTimer( &timers, pb_TimerID_IO );
  for (k = 1; k < nbins+1; k++)
    {
      fprintf(outfile, "%d\n%d\n%d\n", DD[k], DRS[k], RRS[k]);
    }

  if(outfile != stdout)
    fclose(outfile);

  // free memory
  free(data);
  free(random);
  free(binb);
  free(DD);
  free(RRS);
  free(DRS);

  pb_SwitchToTimer( &timers, pb_TimerID_NONE );
  pb_PrintTimerSet( &timers );
  pb_FreeParameters( params );
}

